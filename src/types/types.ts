export interface ISentenceEdit {
  key: string;
  value: string;
}

export interface ISentence {
  who: string;
  what: string;
  where: string;
  when: string;
}

export interface ISentenceObj extends ISentence {
  id: string;
  favorite?: boolean;
}

export interface ISentencesState {
  sentences: ISentenceObj[];
}


export interface ISentenceFavorite {
  id: string;
  favorite: boolean;
}