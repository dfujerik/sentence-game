import { Outlet, Link, useLocation } from "react-router-dom";

import styled from "styled-components";
import colors from "constant/colors";

interface ICustomLinkWrapper {
  active?: boolean;
}

const AppBar = styled.div`
  background-color: ${colors.secondary};
  padding: 0.5em 4em;
  display: flex;
  justify-content: space-between;
`;

const Title = styled.h1`
  font-size: 2em;
  margin: 0;
  color: white;
`;

const Nav = styled.nav`
  display: flex;
  align-items: center;

  > * {
    &:not(:last-child) {
      margin-right: 20px;
    }
  }
`;

const CustomLink = styled(Link)`
  color: ${colors.white};
  text-decoration: inherit;
  font-weight: 500;
  font-size: 1.2em;
`;

const CustomLinkWrapper = styled.div<ICustomLinkWrapper>`
  text-decoration: ${(p) => (p.active ? "underline" : "none")};
`;

const Default = () => {
  const { pathname } = useLocation();

  return (
    <>
      <AppBar>
        <Title>Sentence Game</Title>
        <Nav>
          <CustomLinkWrapper active={pathname === "/"}>
            <CustomLink to="/">Home</CustomLink>
          </CustomLinkWrapper>
          <CustomLinkWrapper active={pathname === "/old-sentences"}>
            <CustomLink to="/old-sentences">My old sentences</CustomLink>
          </CustomLinkWrapper>
        </Nav>
      </AppBar>
      <Outlet />
    </>
  );
};

export default Default;
