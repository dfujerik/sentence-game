import React from "react";

import styled from "styled-components";

import CreateSentence from "components/modules/CreateSentence";

const Wrapper = styled.div`
  padding: 1em;
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const Home = () => {
  return (
    <Wrapper>
      <CreateSentence />
    </Wrapper>
  );
};

export default Home;
