import React from "react";

import { useAppSelector, useAppDispatch } from "hooks/useRedux";
import styled from "styled-components";

import { favorite, remove, sentencesList } from "redux/reducers/sentences";
import SentenceCard from "components/modules/SentenceCard";
import colors from "constant/colors";

import { ISentenceFavorite } from "types/types";

const Wrapper = styled.div`
  padding: 1em;
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const NoData = styled.h3`
  color: ${colors.white}
`;

const OldSentences = () => {
  const { sentences } = useAppSelector(sentencesList);
  const dispatch = useAppDispatch();

  const handleFavorite = (likeValues: ISentenceFavorite) => dispatch(favorite(likeValues));

  const handleRemove = (id: string) => dispatch(remove(id));

  return (
    <Wrapper>
      {sentences
        .map((sentence, index) => (
          <SentenceCard
            key={`sentenceList-${index}`}
            sentence={sentence}
            onFavorite={handleFavorite}
            onDelete={handleRemove}
          />
        ))}
      {sentences.length === 0 && <NoData>No senteces were created</NoData>}
    </Wrapper>
  );
};

export default OldSentences;