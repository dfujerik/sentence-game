import React, { useState, useEffect } from "react";

import styled from "styled-components";
import { useAppSelector, useAppDispatch } from "hooks/useRedux";
import useKeyPress from "hooks/useKeyPress";
import shortid from "shortid";

import useStep from "hooks/useStep";
import TextField from "components/elements/TextField";
import Button from "components/elements/Button";
import { edit, init, sentence } from "redux/reducers/sentence";
import { add } from "redux/reducers/sentences";

import colors from "constant/colors";

const Wrapper = styled.div`
  background-color: ${colors.white};
  padding: 1em;
  border-radius: 3px;
`;

const ButtonWrapper = styled.div`
  padding-top: 1em;
  display: flex;
  justify-content: space-around;
`;

const Stepper = styled.div`
  display: flex;
  padding-bottom: 1em;
  justify-content: center;
  > * {
    &:not(:first-child) {
      padding-left: 1em;
    }
  }
`;

const StepperItem = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const Title = styled.h4`
  margin: 0;
`;

const Error = styled.h5`
  font-weight: 400;
  margin: 0;
  color: red;
`;

const CreateSentence = () => {
  const dispatch = useAppDispatch();
  const currentSentence = useAppSelector(sentence);
  const keyPressEnter = useKeyPress("Enter");

  const { steps, activeStep, isFirstStep, isLastStep, nextStep, backStep, goToStep } = useStep([
    "who",
    "what",
    "where",
    "when",
  ]);
  const [isError, setIsError] = useState<boolean>(false);
  const [isDone, setIsDone] = useState<boolean>(false);

  useEffect(() => {
    if (keyPressEnter) {
      isLastStep ? handleDone() : nextStep();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [keyPressEnter]);

  const handleChange = (value: string) => {
    if (isError) setIsError(false);
    dispatch(edit({ key: steps[activeStep], value }));
  };

  const handleDone = () => {
    if (Object.values(currentSentence).some((x) => x === "")) setIsError(true);
    else {
      dispatch(add({ id: shortid.generate(), ...currentSentence }));
      setIsDone(true);
    }
  };

  const handleAgain = () => {
    dispatch(init());
    setIsDone(false);
    goToStep(0);
  };

  const formatStep = (key: string) => {
    return `${key.charAt(0).toUpperCase() + key.slice(1)}?`;
  };

  const printResult = () => {
    let result = "";

    for (const key in currentSentence) {
      result += result
        ? ` ${currentSentence[key as keyof typeof currentSentence]}`
        : currentSentence[key as keyof typeof currentSentence];
    }

    return result;
  };

  return (
    <Wrapper>
      <Stepper>
        {steps.map((step, index) => (
          <StepperItem key={`${step}-${index}`}>
            <div>{formatStep(step)}</div>
            <div>{currentSentence[step as keyof typeof currentSentence] ? "✔️" : "❌"}</div>
          </StepperItem>
        ))}
      </Stepper>
      {!isDone && (
        <div>
          <Title>Please answer</Title>
          <TextField
            label={formatStep(steps[activeStep])}
            value={currentSentence[steps[activeStep] as keyof typeof currentSentence]}
            onChange={handleChange}
          />
          {isError && <Error>Please complete all questions</Error>}
        </div>
      )}
      {isDone && (
        <div>
          <Title>Result is</Title>
          {printResult()}
        </div>
      )}
      <ButtonWrapper>
        {!isDone && (
          <>
            {!isFirstStep && (
              <Button color={colors.secondary} onClick={backStep}>
                {"Back"}
              </Button>
            )}
            {isLastStep ? (
              <Button color={colors.secondary} onClick={handleDone}>
                {"Done"}
              </Button>
            ) : (
              <Button color={colors.secondary} onClick={nextStep}>
                {"Next"}
              </Button>
            )}
          </>
        )}
        {isDone && (
          <Button color={colors.secondary} onClick={handleAgain}>
            {"Again"}
          </Button>
        )}
      </ButtonWrapper>
    </Wrapper>
  );
};

export default CreateSentence;
