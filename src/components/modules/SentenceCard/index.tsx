import React from "react";

import styled from "styled-components";

import Button from "components/elements/Button";
import colors from "constant/colors";

import { ISentenceObj, ISentenceFavorite } from "types/types";

const Card = styled.div`
  padding: 1em;
  margin-top: 1em;
  background-color: white;
  width: 50%;
  box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Title = styled.h3`
  margin: 0;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

interface IProps {
  sentence: ISentenceObj;
  onFavorite: (favoriteValues: ISentenceFavorite) => void;
  onDelete: (id: string) => void;
}

const SentenceCard = ({ sentence, onFavorite, onDelete }: IProps) => {
  const handleFavoriteClick = () => onFavorite({ id: sentence.id, favorite: !sentence.favorite });

  const handleDeleteClick = () => onDelete(sentence.id);

  return (
    <Card>
      <Header>
        <Title>{`${sentence.who} ${sentence.what} ${sentence.where} ${sentence.when}`}</Title>
        <Button color={colors.secondary} onClick={handleFavoriteClick}>
          {sentence.favorite ? "💛" : "🤍"}
        </Button>
      </Header>
      <Button color={colors.secondary} onClick={handleDeleteClick}>
        {"Delete"}
      </Button>
    </Card>
  );
};

export default SentenceCard;
