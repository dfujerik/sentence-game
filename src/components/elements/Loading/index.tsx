import React from "react";
import styled, { keyframes } from "styled-components";
import colors from "constant/colors";

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

const Wrapper = styled.div`
  height: 100vh;
  justify-content: center;
  align-items: center;
  display: flex;
`;

const Loader = styled.div`
  border: 5px solid ${colors.white};
  border-top: 5px solid ${colors.secondary};
  border-radius: 50%;
  width: 50px;
  height: 50px;
  animation: ${rotate} 1s linear infinite;
`;

const Loading = () => {
  return (
    <Wrapper>
      <Loader />
    </Wrapper>
  );
};

export default Loading;
