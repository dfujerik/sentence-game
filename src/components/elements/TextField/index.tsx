import React from "react";
import styled from "styled-components";

const FieldWrapper = styled.div`
  display: flex;
  margin: 0.5em 0;

  > * {
    &:nth-child(2) {
      margin-left: 2em;
      width: 100%;
    }
  }
`;

const Label = styled.h3`
  font-weight: 400;
  margin: 0;
`;

const InputWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

interface IProps {
  label: string;
  value: string;
  onChange: (value: string) => void;
}

const TextField = ({ label, value, onChange }: IProps) => {
  const handleChange = (event: any) => onChange(event.target.value ?? "");

  return (
    <FieldWrapper>
      <Label>{`${label}:`}</Label>
      <InputWrapper>
        <input autoComplete="off" name={label} value={value} onChange={handleChange}></input>
      </InputWrapper>
    </FieldWrapper>
  );
};

export default TextField;
