import styled from "styled-components";
import colors from "constant/colors"

const Button = styled.button<{ color?: string, disabled?: boolean }>`
  background: ${props => props.disabled ? "gray" : props.color ?? colors.primary };
  color: white;
  font-size: 1em;
  padding: 0.25em 1em;
  border: 2px solid ${props => props.color ?? colors.primary};
  border-radius: 3px;
  width: fit-content;
`;

export default Button;
