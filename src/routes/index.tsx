import { useRoutes } from "react-router-dom";

import Default from "components/layout/Default";

import Home from "components/pages/Home";
import OldSentences from "components/pages/OldSentences";

const AppRoutes = () => {
  const routes = {
    path: "/",
    element: <Default />,
    children: [
      {
        path: "/",
        element: <Home />,
      },
      {
        path: "/old-sentences",
        element: <OldSentences />,
      },
    ],
  };

  return useRoutes([routes]);
};

export default AppRoutes;
