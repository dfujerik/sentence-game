import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "store";
import { ISentence, ISentenceEdit } from "types/types";

const initialState: ISentence = {
  who: "",
  what: "",
  where: "",
  when: "",
};

export const sentenceSlice = createSlice({
  name: "sentence",
  initialState,
  reducers: {
    edit: (state, action: PayloadAction<ISentenceEdit>) => {
      let { key, value } = action.payload;

      if (key in state) state[key as keyof typeof state] = value;
    },
    init: () => initialState,
  },
});

export const { edit, init } = sentenceSlice.actions;

export const sentence = (state: RootState) => state.sentence;

export default sentenceSlice.reducer;
