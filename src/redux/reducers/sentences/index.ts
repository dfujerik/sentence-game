import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "store";

import { ISentencesState, ISentenceObj, ISentenceFavorite } from "types/types";

const initialState: ISentencesState = {
  sentences: [],
};

export const sentencesSlice = createSlice({
  name: "sentences",
  initialState,
  reducers: {
    add: (state, action: PayloadAction<ISentenceObj>) => {
      state.sentences.push(action.payload);
    },
    remove: (state, action: PayloadAction<string>) => {
      const index = state.sentences.findIndex((sente) => sente.id === action.payload);

      if (index !== -1) state.sentences.splice(index, 1);
    },
    favorite: (state, action: PayloadAction<ISentenceFavorite>) => {
      let { favorite, id } = action.payload;
      const index = state.sentences.findIndex((sente) => sente.id === id);

      if (index !== -1) state.sentences[index].favorite = favorite;
    },
  },
});

export const { add, remove, favorite } = sentencesSlice.actions;

export const sentencesList = (state: RootState) => state.sentences;

export default sentencesSlice.reducer;
