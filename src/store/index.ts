import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import sentencesReducer from "redux/reducers/sentences";
import sentenceReducer from "redux/reducers/sentence";
import storage from "redux-persist/lib/storage";
import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";

const reducers = combineReducers({
  sentences: sentencesReducer,
  sentence: sentenceReducer,
});

const persistConfig = {
  key: "root",
  blacklist: ['sentence'],
  storage,
};

const persistedReducer = persistReducer(persistConfig, reducers);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
