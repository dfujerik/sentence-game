const colors = {
  primary: "#264653",
  secondary: "#2A9D8F",
  yellow: "#E9C46A",
  orange: "#F4A261",
  red: "#E76F51",
  white: "#f3f3f3",
};

export default colors;
